//function that return items which only contain vitamin c
function onlyVitaminC(array) {
  //applying filter to catch items that only have vitamin c
  let vitaminCItems = array.filter((item, index, array) => {
    return (
      item.contains.includes("Vitamin C") === true &&
      item.contains.split(", ").length === 1
    );
  });

  return vitaminCItems;
}

//exporting the above function
module.exports = onlyVitaminC;
