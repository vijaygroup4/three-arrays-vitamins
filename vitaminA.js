//function that return items that have vitamin A
function vitaminA(array) {
  //applying filter to catch items that have vitamin A
  let vitaminAItems = array.filter((item, index, array) => {
    return item.contains.includes("Vitamin A") === true;
  });

  return vitaminAItems;
}

//exporting the above function
module.exports = vitaminA;
