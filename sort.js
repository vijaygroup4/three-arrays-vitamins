//function to compare two items and give result in ascending order
function compare(item1, item2) {
  let item1Vitamins = item1.contains.split(", ");
  let item2Vitamins = item2.contains.split(", ");

  //finding the difference value
  let difference = item1Vitamins.length - item2Vitamins.length;
  return difference;
}

//function to sort the array
function sort(array) {
  //applying sort method to sort and taking above compare function to sort
  let sortedItems = array.sort((item1, item2) => {
    return compare(item1, item2);
  });
  return sortedItems;
}

//exporting the above function
module.exports = sort;
