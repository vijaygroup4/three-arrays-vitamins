//importing items array
let items = require("./items");

//importing vitamin c function
let onlyVitaminC = require("../onlyVitaminC");

let vitaminCItems = onlyVitaminC(items);

console.log(vitaminCItems);
