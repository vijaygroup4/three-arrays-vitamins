//importing items array
let items = require("./items");

//importing sort function
let sort = require("../sort");

let sortedItems = sort(items);

console.log(sortedItems);
