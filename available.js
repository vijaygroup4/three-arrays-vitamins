//function that returns available Items
function available(array) {
  //filtering the items which are available
  let availableItems = array.filter((item, index, array) => {
    return item.available === true;
  });

  return availableItems;
}

//exporting the above function
module.exports = available;
