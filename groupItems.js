//function to group Items based on vitamins
function groupItems(array) {
  //applying reduce method to group items
  let groupedObject = array.reduce(
    (accumulator, currentValue, index, array) => {
      //applying multiple if statements to search for specified vitamin and store item
      if (currentValue.contains.includes("Vitamin A")) {
        accumulator["Vitamin A"].push(currentValue.name);
      }
      if (currentValue.contains.includes("Vitamin B")) {
        accumulator["Vitamin B"].push(currentValue.name);
      }
      if (currentValue.contains.includes("Vitamin C")) {
        accumulator["Vitamin C"].push(currentValue.name);
      }
      if (currentValue.contains.includes("Vitamin D")) {
        accumulator["Vitamin D"].push(currentValue.name);
      }
      if (currentValue.contains.includes("Vitamin K")) {
        accumulator["Vitamin K"].push(currentValue.name);
      }

      return accumulator;
    },
    {
      "Vitamin A": [],
      "Vitamin B": [],
      "Vitamin C": [],
      "Vitamin D": [],
      "Vitamin K": [],
    }
  );

  return groupedObject;
}

//exporting the above function
module.exports = groupItems;
